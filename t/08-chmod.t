
require "TEST.pl";
TEST::init();

print "1..2\n";

$tmpfile1 = TEST::tmpfile(<<'EOT');
foo
[FOO:Inside first foo:][BAR:Inside first bar:]
bar
[FOO:Inside second foo[BAR:Inside second bar:]:]
baz
EOT

$tmpfile2 = TEST::tmpfile();

$rc = TEST::system("$ENV{SLICE} -o FOO:$tmpfile2\@o+x,u-r $tmpfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
print (! -r $tmpfile2 ? "ok\n" : "not ok\n");

TEST::cleanup();

