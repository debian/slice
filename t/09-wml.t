
require "TEST.pl";
TEST::init();

print "1..3\n";

$tmpfile1 = TEST::tmpfile(<<'EOT');
<html>
<head>
<title>[EN:Titlepage:][DE:Titelseite:]</title>
</head>
<body>

<center>
<h1>[EN:The Title itself:][DE:Der Titel selbst:]</h1>
</center>

<blockquote>
[EN:...English Abstract...:]
[DE:...Deutsche Zusammenfassung...:]
</blockquote>

[EN:...English Text...:]
[DE:...Deutscher Text...:]

</body>
</html>
EOT

$tmpfile2 = TEST::tmpfile(<<'EOT');
<html>
<head>
<title>Titlepage</title>
</head>
<body>

<center>
<h1>The Title itself</h1>
</center>

<blockquote>
...English Abstract...

</blockquote>

...English Text...


</body>
</html>
EOT
$tmpfile3 = TEST::tmpfile(<<'EOT');
<html>
<head>
<title>Titelseite</title>
</head>
<body>

<center>
<h1>Der Titel selbst</h1>
</center>

<blockquote>

...Deutsche Zusammenfassung...
</blockquote>


...Deutscher Text...

</body>
</html>
EOT

$rc = TEST::system("$ENV{SLICE} -o EN+UNDEF:test.html.en -o DE+UNDEF:test.html.de $tmpfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = TEST::system("cmp $tmpfile2 test.html.en");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = TEST::system("cmp $tmpfile3 test.html.de");
print ($rc == 0 ? "ok\n" : "not ok\n");

push(@TEST::TMPFILES, 'test.html.en');
push(@TEST::TMPFILES, 'test.html.de');

TEST::cleanup();

