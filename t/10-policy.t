
require "TEST.pl";
TEST::init();

print "1..9\n";

$tmpfile1 = TEST::tmpfile(<<'EOT');
foo
bar
EOT

$tmpfile2 = TEST::tmpfile(<<'EOT');
foo
bar
EOT

$rc = TEST::system("$ENV{SLICE} -o EN+UNDEF:test.u1 -o A*+UNDEF:test.w1 $tmpfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = TEST::system("cmp $tmpfile2 test.u1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = TEST::system("cmp $tmpfile2 test.w1");
print ($rc == 0 ? "ok\n" : "not ok\n");

$rc = TEST::system("$ENV{SLICE} -o EN+UNDEF:test.u2#u2 -o A*+UNDEF:test.w2#w2 $tmpfile1 2>/dev/null");
print ($rc == 0 ? "ok\n" : "not ok\n");
print (! -f 'test.u2' ? "ok\n" : "not ok\n");
print (! -f 'test.w2' ? "ok\n" : "not ok\n");

$rc = TEST::system("$ENV{SLICE} -o EN+UNDEF:test.u3#u3 -o A*+UNDEF:test.w3#w3 $tmpfile1 2>/dev/null");
print ($rc == 256 ? "ok\n" : "not ok\n");
print (! -f 'test.u3' ? "ok\n" : "not ok\n");
print (! -f 'test.w3' ? "ok\n" : "not ok\n");

push(@TEST::TMPFILES, 'test.u1', 'test.u2', 'test.u3');
push(@TEST::TMPFILES, 'test.w1', 'test.w2', 'test.w3');

TEST::cleanup();

