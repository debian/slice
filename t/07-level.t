
require "TEST.pl";
TEST::init();

print "1..2\n";

$tmpfile1 = TEST::tmpfile(<<'EOT');
foo
[FOO:Inside first foo:][BAR:Inside first bar:]
bar
[FOO:Inside second foo
[BAR:Inside second bar:]:]
baz
EOT

$tmpfile2 = TEST::tmpfile(<<'EOT');
Inside first fooInside second foo
EOT
$tmpfile3 = TEST::tmpfile();

$rc = TEST::system("$ENV{SLICE} -o FOO\@:$tmpfile3 $tmpfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = TEST::system("cmp $tmpfile2 $tmpfile3");
print ($rc == 0 ? "ok\n" : "not ok\n");

TEST::cleanup();

